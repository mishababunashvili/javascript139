/* multiline comment
რიცხვებზე მოქმედებები:
+
-
*
/

% - modulo (ნაშთის ოპერატორი)
** - exponent (ახარისხება)
*/
// single line comment

console.log(5 + 6); // 11
console.log(11 - 9); // 2
console.log(11 * 2); // 22
console.log(33 / 3); // 11
console.log(5 % 2); // 1
console.log(5 ** 2); // 25

/*
სტრინგებზე მოქმედებები
+
*/

// concatenation
console.log("Hello" + " " + "World");
