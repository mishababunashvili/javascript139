const userName = "JOE";

console.log(
    userName.toLocaleLowerCase() === "joe" 
    ? 'Hello ' + userName 
    : 'Bye ' + userName
);

// if (userName.toLocaleLowerCase() === "joe") {
//     console.log('Hello ' + userName);
// } else {
//     console.log('Bye ' + userName);
// }