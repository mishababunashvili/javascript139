const userName = "Joe";
const lastname = "Doe";
const age = 45;
/*
console.log(userName + ' ' + lastname + ' ' + String(age));

console.log(String(5) + String(5));
console.log(String(true) + "false");
// console.log(true + true + true);
*/

// boolean casting (Truthy / Falsy)

/*
console.log(Boolean(userName)); //
console.log(Boolean(""));
console.log(Boolean(-1000)); 
console.log(Boolean(1000));
console.log(Boolean(1));
console.log(Boolean(0.000000000001));
console.log(Boolean(0));
*/

// numbers
const someNumber = "-900.1wecwe";

console.log(parseInt(someNumber));
console.log(parseFloat(someNumber));
console.log(Number(someNumber));
console.log(Number(true), Number(false));

